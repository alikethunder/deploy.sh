cd /home/tvcamtv/tvcamtv_rep
git pull origin master
meteor npm install
NODE_TLS_REJECT_UNAUTHORIZED=0 meteor build /home/tvcamtv/tvcamtv --architecture os.linux.x86_64 --allow-superuser
echo "Project is built"
cd /home/tvcamtv/tvcamtv
tar -xf tvcamtv_rep.tar.gz
cd bundle/programs/server
npm install 
systemctl restart tvcamtv
cd /home/tvcamtv/tvcamtv
rm tvcamtv_rep.tar.gz
echo "Deployed"
